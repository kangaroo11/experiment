package com.kangaroo;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by 123 on 2017/12/11.
 */
@Data
@AllArgsConstructor
public class UserLombok extends Person{
    private String name;
    private Integer age;
    private String sex;

}
