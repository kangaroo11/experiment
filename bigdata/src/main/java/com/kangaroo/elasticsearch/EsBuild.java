package com.kangaroo.elasticsearch;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * @author ;liub
 * @Description:
 * @Date: 2018-02-09 11:52
 **/

@Slf4j
@Service
public class EsBuild {


    @Autowired
    private TransportClient client;

    /**
     * @description:建立索引
     * rest示例：PUT /index1
     * {
     * "mappings": {
     * "type1":{
     * "   properties": {
     * "name":{
     * "type": "text",
     * "analyzer": "standard",
     * "store": true,
     * "search_analyzer": "standard"
     * }
     * }
     * }
     * }
     * }
     * @params:
     * @time;16:07 2018/2/13
     **/
    public String buileIndex(String index) {
        CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate(index);
        XContentBuilder mapping_json = null;
        try {
            mapping_json = jsonBuilder()
                    .startObject()
                    .startObject("properties")
                    .startObject("title")
                    .field("type", "text")
                    .field("analyzer", "standard")  //分词器
                    .field("store", true) //存储到索引文件中
                    .field("search_analyzer", "standard") //查询使用的分词器
                    .endObject()
                    .startObject("nmuber")
                    .field("type", "integer")
                    .endObject()
                    .endObject()
                    .endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        CreateIndexResponse response = createIndexRequestBuilder.addMapping("car", mapping_json).get();
        return response.toString();
    }

    /**
     * @description:插入数据
     * @params:
     * @time;16:35 2018/2/13
     **/
    public String addData(String index, String type, String id, String data) throws IOException {
        IndexResponse response = client.prepareIndex("index1", "type1")
                .setSource(jsonBuilder()
                        .startObject()
                        .field("name", "trying out Elasticsearch")
                        .endObject(), XContentType.JSON)
                .get();
        return response.toString();
    }



}