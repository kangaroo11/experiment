package com.kangaroo.csv;

/**
 * @author liubo
 * @ClassName TestEntity
 * @Description TODO
 * @Date 2020/12/7
 * @since JDK 1.8
 */
public class TestEntity {

    public String a;

    public Integer b;

    public String c;

    public String d;

    public Integer e;

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }
}
