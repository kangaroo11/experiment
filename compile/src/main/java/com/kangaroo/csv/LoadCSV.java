package com.kangaroo.csv;

import com.alibaba.fastjson.JSON;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.IOException;
import java.util.List;

/**
 * @author liubo
 * @ClassName LoadCSV
 * @Description TODO
 * @Date 2020/10/17
 * @since JDK 1.8
 */
public class LoadCSV {
    public static void main(String[] args) throws IOException {
        /*
        File csvFile = new File("F:\\IDEA_JAVA\\CSV\\test\\test.csv");
        InputStream fi = new FileInputStream(csvFile);
        ANTLRInputStream inputStream = new ANTLRInputStream(fi);
        CSVLexer lexer = new CSVLexer(inputStream);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        CSVParser parser = new CSVParser(tokenStream);
        ParseTreeWalker walker = new ParseTreeWalker();
        Loader loader = new Loader();
        walker.walk(loader, parser.file());
        System.out.println(loader.rows);
        */
        String path = "E:\\test.csv";
        CharStream inputStream = CharStreams.fromFileName(path);
        CSVLexer lexer = new CSVLexer(inputStream);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        CSVParser parser = new CSVParser(tokenStream);
        ParseTreeWalker walker = new ParseTreeWalker();
        Loader loader = new Loader();
        walker.walk(loader, parser.file());
        List t=loader.rows;
//        System.out.println(JSON.toJSONString(t.get(0)));
        String sss=JSON.toJSONString(t.get(0));
        TestEntity testEntity= JSON.parseObject(sss, TestEntity.class);
//        System.out.println(loader.rows);?
    }
}

