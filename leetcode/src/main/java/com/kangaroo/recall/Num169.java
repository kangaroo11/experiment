package com.kangaroo.recall;

/**
 * @author liubo
 * @ClassName Num169
 * @Description TODO
 * @Date 2020/12/8
 * @since JDK 1.8
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数大于 ⌊ n/2 ⌋ 的元素。
 * <p>
 * 你可以假设数组是非空的，并且给定的数组总是存在多数元素。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/majority-element
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * <p>
 * 示例 1:
 * <p>
 * 输入: [3,2,3]
 * 输出: 3
 * 示例 2:
 * <p>
 * 输入: [2,2,1,1,1,2,2]
 * 输出: 2
 */
public class Num169 {


    public static void solution(Integer[] source) {
        int size = source.length;
        int half = size / 2;

        Map<Integer, Integer> map = new HashMap<>();
        for (Integer item : source) {
            Integer v = map.get(item);
            if (v == null) {
                map.put(item, 1);
                continue;
            }
            v = v + 1;
            map.put(item, v);
            if(v>half ){
                System.out.println(item);
                break;
            }
        }
    }

    public static void main(String[] args) {
        Integer[] source=new Integer[]{3,2,3};
        solution(source);

    }


}
