本模块主要用于学习mybatis、jdbc相关知识

https://my.oschina.net/kailuncen/blog/1334771

mybatis一级缓存：默认开启
在excutor时，会判断sql查询是否有缓存。
在sqlSession内部完成，即仅能完成sqlSession内部的缓存，一旦一个sqlSession修改，而另一个sqlsession则可能读取脏数据，建议不要使用一级缓存。

mybatis二级缓存：默认关闭
在excutor之前，执行cacheexcutor，判断是否有缓存，基于namespace，一旦存在多表关联查询就可能出现脏数据。
在sqlsession之间做到缓存共享，
