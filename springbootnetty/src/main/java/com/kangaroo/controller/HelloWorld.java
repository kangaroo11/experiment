package com.kangaroo.controller;

import com.kangaroo.dao.mydao;
import com.kangaroo.entity.StudentEntity;
import com.kangaroo.netty.Global;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.*;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.SocketAddress;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Created by 123 on 2017/11/28.
 */
@RestController
public class HelloWorld {

    ConcurrentHashMap<String,Object> map=new ConcurrentHashMap();



    @Autowired
    @Qualifier("readSqlSession")
    private SqlSessionTemplate readSqlSession;

    @Autowired
    @Qualifier("writeSqlSession")
    private SqlSessionTemplate writeSqlSession;

    @Autowired
    private mydao mdao;

    @RequestMapping("ts")
    public void t1(){
        List list =readSqlSession.selectList("com.kangaroo.mapper.StudentMapper.getStudent");
        StudentEntity e=new StudentEntity();
        e.setName("hello2");
        e.setAge(23);
//        Integer t2=readSqlSession.insert("com.kangaroo.mapper.StudentMapper.addStudent",e);
        Integer t=writeSqlSession.insert("com.kangaroo.mapper.StudentMapper.addStudent",e);
        List list2 =readSqlSession.selectList("com.kangaroo.mapper.StudentMapper.getStudent");
        System.out.println("dfsa");

    }

    @RequestMapping("ts2")
    public void t2(){
        List list= mdao.read();
        Integer t= mdao.write();
        List list2=mdao.read();
        System.out.println("");
    }


    @RequestMapping("/hello")
    public String hello(@RequestParam String param){
        System.out.println("hlls");
        Channel channel=Global.map.get("user1");
        for(int i=0;i<1000000;i++){
            Channel channel2=new Channel() {
                @Override
                public ChannelId id() {
                    return null;
                }

                @Override
                public EventLoop eventLoop() {
                    return null;
                }

                @Override
                public Channel parent() {
                    return null;
                }

                @Override
                public ChannelConfig config() {
                    return null;
                }

                @Override
                public boolean isOpen() {
                    return false;
                }

                @Override
                public boolean isRegistered() {
                    return false;
                }

                @Override
                public boolean isActive() {
                    return false;
                }

                @Override
                public ChannelMetadata metadata() {
                    return null;
                }

                @Override
                public SocketAddress localAddress() {
                    return null;
                }

                @Override
                public SocketAddress remoteAddress() {
                    return null;
                }

                @Override
                public ChannelFuture closeFuture() {
                    return null;
                }

                @Override
                public boolean isWritable() {
                    return false;
                }

                @Override
                public Unsafe unsafe() {
                    return null;
                }

                @Override
                public ChannelPipeline pipeline() {
                    return null;
                }

                @Override
                public ByteBufAllocator alloc() {
                    return null;
                }

                @Override
                public ChannelPromise newPromise() {
                    return null;
                }

                @Override
                public ChannelProgressivePromise newProgressivePromise() {
                    return null;
                }

                @Override
                public ChannelFuture newSucceededFuture() {
                    return null;
                }

                @Override
                public ChannelFuture newFailedFuture(Throwable cause) {
                    return null;
                }

                @Override
                public ChannelPromise voidPromise() {
                    return null;
                }

                @Override
                public ChannelFuture bind(SocketAddress localAddress) {
                    return null;
                }

                @Override
                public ChannelFuture connect(SocketAddress remoteAddress) {
                    return null;
                }

                @Override
                public ChannelFuture connect(SocketAddress remoteAddress, SocketAddress localAddress) {
                    return null;
                }

                @Override
                public ChannelFuture disconnect() {
                    return null;
                }

                @Override
                public ChannelFuture close() {
                    return null;
                }

                @Override
                public ChannelFuture bind(SocketAddress localAddress, ChannelPromise promise) {
                    return null;
                }

                @Override
                public ChannelFuture connect(SocketAddress remoteAddress, ChannelPromise promise) {
                    return null;
                }

                @Override
                public ChannelFuture connect(SocketAddress remoteAddress, SocketAddress localAddress, ChannelPromise promise) {
                    return null;
                }

                @Override
                public ChannelFuture disconnect(ChannelPromise promise) {
                    return null;
                }

                @Override
                public ChannelFuture close(ChannelPromise promise) {
                    return null;
                }

                @Override
                public Channel read() {
                    return null;
                }

                @Override
                public ChannelFuture write(Object msg) {
                    return null;
                }

                @Override
                public ChannelFuture write(Object msg, ChannelPromise promise) {
                    return null;
                }

                @Override
                public Channel flush() {
                    return null;
                }

                @Override
                public ChannelFuture writeAndFlush(Object msg, ChannelPromise promise) {
                    return null;
                }

                @Override
                public ChannelFuture writeAndFlush(Object msg) {
                    return null;
                }

                @Override
                public <T> Attribute<T> attr(AttributeKey<T> key) {
                    return null;
                }

                @Override
                public int compareTo(Channel o) {
                    return 0;
                }
            };
            String key="us"+String.valueOf(i);
             map.put(key,channel2);
        }
        channel.writeAndFlush(new TextWebSocketFrame("hello,"+param));
//        Global.map.get("user1").writeAndFlush(new TextWebSocketFrame("hello"));
        return param;
    }

}
